/* This is a simple script which listens for tasks
    on a RabbitMQ queue and processes them. Based on MultiplierService. */

var amqp = require('amqplib/callback_api');

// URL for RabbitMQ server
var queueserver = process.env.AMQP_SERVER;
// If not defined, exit
if(typeof(queueserver)=="undefined" || !queueserver) {
    console.error("AMQP server URL not defined in environment parameters.");
    process.exit();
}

// In/out queues
var in_queue = 'simpletasks';
var out_queue = 'simpletasksout';

const buf = Buffer.from(Math.floor(Math.random() * 100000).toString());
servername = buf.toString('base64');

console.log("Connecting to "+queueserver.substr(queueserver.indexOf("@")+1));

amqp.connect(queueserver, function(err, conn) {
    console.log("Connected. Creating channel...");
    conn.createChannel(function(err, ch) {
        console.log("Asserting input/output queues...");
        ch.assertQueue(in_queue, {durable: false});
        ch.assertQueue(out_queue, {durable: false});

        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", in_queue);
        ch.consume(in_queue, function(msg) {
            console.log(" [x] Received %s", msg.content.toString());
            var receivedobject = JSON.parse(msg.content); 
            console.log("Sending  response...");
            if(receivedobject.task == "multiply")
                ch.sendToQueue(out_queue, new Buffer(JSON.stringify(handleMultiplyTask(receivedobject))));
            else if(receivedobject.task == "divide")
                ch.sendToQueue(out_queue, new Buffer(JSON.stringify(handleDivideTask(receivedobject))));
            else if(receivedobject.task == "add")
                ch.sendToQueue(out_queue, new Buffer(JSON.stringify(handleAddTask(receivedobject))));
            else if(receivedobject.task == "subtract")
                ch.sendToQueue(out_queue, new Buffer(JSON.stringify(handleSubtractTask(receivedobject))));
            else {
                console.log("Error!");
                ch.sendToQueue(out_queue, new Buffer("Error!"));
            }

        }, {noAck: true});
    });
});

function handleMultiplyTask(task) {
    var output = {};
    output.result = task.parms[0] * task.parms[1];
    output.server = servername;
    return output;
}
function handleDivideTask(task) {
    var output = {};
    output.result = task.parms[0] / task.parms[1];
    output.server = servername;
    return output;
}
function handleAddTask(task) {
    var output = {};
    output.result = task.parms[0] + task.parms[1];
    output.server = servername;
    return output;
}
function handleSubtractTask(task) {
    var output = {};
    output.result = task.parms[0] - task.parms[1];
    output.server = servername;
    return output;
}